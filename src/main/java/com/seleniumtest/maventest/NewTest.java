package com.seleniumtest.maventest;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class NewTest {

	ChromeDriver driver;
  
  @BeforeClass
  public void beforeClass() {
	  
	  System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
	   driver = new ChromeDriver();
  }
  
  @Test
  public void logInTestCase() throws InterruptedException {
	  driver.get("http://www.fb.com");
	  driver.findElement(By.id("email")).sendKeys("ram@gmail.com");
	  
	  //entering password credentials
	  driver.findElement(By.id("pass")).sendKeys("abc123");
	  
	  //click on login button
	  WebElement loginBtn = driver.findElement(By.id("u_0_2"));
	  loginBtn.click();
	  
	 WebDriverWait wait = new WebDriverWait(driver, 10);
	 wait.until(ExpectedConditions.presenceOfElementLocated(By.id("u_0_2")));
	  
	  WebElement errbox =driver.findElement(By.id("error_box"));
	    
	 Assert.assertTrue(errbox.getText().contains("Please try again later"));
  }
  

  @AfterClass
  public void afterClass() {
	  
  }

}
